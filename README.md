# ttm.sh scripts

Some scripts to make using ttm.sh easier!

# Usage:

Examples:

Shorten Link: `./shorten.sh https://google.com.au`

Upload File: `./upload.sh /home/kiiwii/file.png`

Upload Remote File: `./remote.sh https://cdn.discordapp.com/attachments/759635320496586752/808096469789048902/Z.png`

# TODO:

- Combine all to one file
- Add support for other domains